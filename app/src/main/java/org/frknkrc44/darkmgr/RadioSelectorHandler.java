package org.frknkrc44.darkmgr;

import android.app.UiModeManager;
import android.content.res.Configuration;

public class RadioSelectorHandler {
	
	private RadioSelectorHandler(){}
	
	public static final int indexToId(int index){
		switch(index){
			case UiModeManager.MODE_NIGHT_NO:
				return android.R.id.button1;
			case UiModeManager.MODE_NIGHT_YES:
				return android.R.id.button2;
			case UiModeManager.MODE_NIGHT_AUTO:
				return android.R.id.button3;
		}
		return -1;
	}
	
	public static final int idToIndex(int id){
		switch(id){
			case android.R.id.button1:
				return UiModeManager.MODE_NIGHT_NO;
			case android.R.id.button2:
				return UiModeManager.MODE_NIGHT_YES;
			case android.R.id.button3:
				return UiModeManager.MODE_NIGHT_AUTO;
		}
		return -1;
	}
	
	public static final int boolToId(boolean night){
		return indexToId(night 
								? UiModeManager.MODE_NIGHT_YES
								: UiModeManager.MODE_NIGHT_NO
						);
	}
	
	public static final int boolToIndex(boolean night){
		return idToIndex(boolToId(night));
	}
	
}
