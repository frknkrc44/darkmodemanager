package org.frknkrc44.darkmgr;

import android.app.UiModeManager;
import android.content.Context;

public class UiModeHandler {
	
	private UiModeHandler(){}
	
	public static final int getNightModeStatus(Context ctx){
		UiModeManager uiModeMgr = (UiModeManager) ctx.getSystemService(Context.UI_MODE_SERVICE);
		return uiModeMgr.getNightMode();
	}
	
	public static final void setNightModeStatus(Context ctx, int mode){
		UiModeManager uiModeMgr = (UiModeManager) ctx.getSystemService(Context.UI_MODE_SERVICE);
		uiModeMgr.setNightMode(mode);
	}
	
}
