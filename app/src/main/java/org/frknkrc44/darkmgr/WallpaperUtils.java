package org.frknkrc44.darkmgr;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;

public class WallpaperUtils {
	
	private WallpaperUtils(){}
	
	public static final boolean isWallpaperLight(Context ctx){
		WallpaperManager wallMgr = (WallpaperManager) ctx.getSystemService(Context.WALLPAPER_SERVICE);
		BitmapDrawable wallDrw = (BitmapDrawable) wallMgr.getDrawable();
		Bitmap wall = wallDrw.getBitmap();
		int color = BitmapUtils.getBitmapColor(wall);
		return ColorUtils.isColorLight(color);
	}
	
	public static final int getColorModeIdFromWallpaper(Context ctx){
		return RadioSelectorHandler.boolToId(!isWallpaperLight(ctx));
	}
	
	public static final int getColorModeFromWallpaper(Context ctx){
		return RadioSelectorHandler.idToIndex(getColorModeIdFromWallpaper(ctx));
	}
}
