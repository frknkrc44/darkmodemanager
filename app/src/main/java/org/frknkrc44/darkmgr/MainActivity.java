package org.frknkrc44.darkmgr;

import android.Manifest;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends Activity implements OnCheckedChangeListener {
	
	private RadioGroup group;
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
		if(SDK_INT >= 21){
			getActionBar().setElevation(0);
		}
        setContentView(R.layout.main);
		group = (RadioGroup) findViewById(android.R.id.list);
		int id = UiModeHandler.getNightModeStatus(this);
		id = RadioSelectorHandler.indexToId(id);
		group.check(id);
		group.setOnCheckedChangeListener(this);
		if(SDK_INT < 28){
			View autoSwitch = findViewById(android.R.id.button3);
			autoSwitch.setVisibility(View.GONE);
		}
    }
	
	@Override
	public void onCheckedChanged(RadioGroup p1, int p2){
		int index = RadioSelectorHandler.idToIndex(p2);
		UiModeHandler.setNightModeStatus(p1.getContext(),index);
	}
	
	public void selectFromWallpaper(View v){
		if(SDK_INT >= 23 && !checkPermissions()){
			return;
		}
		
		int id = WallpaperUtils.getColorModeIdFromWallpaper(this);
		group.check(id);
	}
	
	private int permissionRequestCount = 0;
	
	private boolean checkPermissions(){
		String perm = Manifest.permission.READ_EXTERNAL_STORAGE;
		boolean ret = checkCallingOrSelfPermission(perm) == PackageManager.PERMISSION_GRANTED;
		if(!ret){
			permissionRequestCount++;
			requestPermissions(new String[]{perm}, 0);
		}
		return ret;
	}

	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if(permissionRequestCount > 10){
			Toast.makeText(this, "Requested permissions don't granted from you and I can't detect wallpaper, sorry :(",Toast.LENGTH_LONG).show();
			permissionRequestCount = 0;
			return;
		}
		selectFromWallpaper(null);
	}
	
}
